from flask import Flask
from flask_restful import Api

from resources.users import UserController, UserListController


def create_app():
    app = Flask(__name__)
    api = Api(app)

    @app.route("/")
    def hello_world():
        return "<p>Hello, World!</p>"

    api.add_resource(UserListController, '/users')
    api.add_resource(UserController, '/users/<string:user_id>')
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0')
