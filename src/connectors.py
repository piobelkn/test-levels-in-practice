import json

from kafka import KafkaProducer

import config


class KafkaConnector:
    kafka_producer = KafkaProducer(bootstrap_servers=config.kafka_brokers,
                                   value_serializer=lambda x: json.dumps(x).encode('utf-8'),
                                   key_serializer=lambda k: k.encode('utf-8'))

    @classmethod
    def send_to_topic(cls, topic_name, value, key):
        cls.kafka_producer.send(topic_name, value=value, key=key)
        cls.kafka_producer.flush()
